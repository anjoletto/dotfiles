# Configuration files for things that I use

TUI:
- neovim
- tmux
- zsh

GUI:
- alacritty
- dunst
- gtk
- rofi
- x11 (xinitrc, user dirs, xresources)
