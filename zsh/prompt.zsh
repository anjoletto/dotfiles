autoload -Uz vcs_info
setopt prompt_subst

# empty line before each prompt, except fist
precmd() { precmd() { print "" } }

# version control information
precmd_functions+=( vcs_info )
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' get-revision true
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' stagedstr '+'
zstyle ':vcs_info:*' unstagedstr '*'
zstyle ':vcs_info:*' formats '%F{#777777}[%b%u%c]'

# set prompt
PROMPT="%(?.%F{gray}.%F{red})%(!.>>.>)%F{gray} "
RPROMPT=$' ${vcs_info_msg_0_} %F{#777777}%~'
