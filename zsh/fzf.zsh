if [ -n "${commands[fzf-share]}" ]; then
  source "$(fzf-share)/key-bindings.zsh"
  source "$(fzf-share)/completion.zsh"
else
  source $ZDOTDIR/fzf_config.zsh
  source $ZDOTDIR/fzf_comp.zsh
fi

export FZF_DEFAULT_OPTS="--layout=reverse --header 'search files and folders' --prompt 'all: ' --bind 'ctrl-d:change-prompt(folders: )+reload(find * -type d)' --bind 'ctrl-f:change-prompt(files: )+reload(find * -type f)'"

export FZF_COMPLETION_TRIGGER='**'
export FZF_COMPLETION_OPTS='--border --info=inline'
