source $ZDOTDIR/options.zsh
source $ZDOTDIR/completion.zsh
source $ZDOTDIR/prompt.zsh
source $ZDOTDIR/envvars.zsh
source $ZDOTDIR/aliases.zsh
source $ZDOTDIR/fzf.zsh
source $ZDOTDIR/zoxide.zsh

if [[ -f $ZDOTDIR/custom.zsh ]]; then
  source $ZDOTDIR/custom.zsh
fi

if [ -e '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh' ]; then
  . '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh'
fi
