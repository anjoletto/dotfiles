alias tmux='tmux -2'
alias ls='eza'
alias l='eza -lhA --git'
alias tree='eza --tree'
alias bat='bat --color never'
alias cat='bat'
alias rd='ripdrag'
alias zmux='zi && tmux'
alias marp-server='marp --html -s .'
alias nix-gens="sudo nix-env -p /nix/var/nix/profiles/system --list-generations"

bindkey -s '^n' '^u$HOME/.dots/scripts/tmuxeagen^M'

