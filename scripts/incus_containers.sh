#!/usr/bin/env sh

selected_container=$(incus list | grep RUNNING | awk '{print $2}' | grep -e "[a-z]" | rofi -dmenu)

if [[ -z $selected_container ]]; then
    exit 0
fi

echo $selected_container > /tmp/incus_container
