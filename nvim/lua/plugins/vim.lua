return {
	{ "fatih/vim-go", ft = "go" },
	{
		"dhruvasagar/vim-table-mode",
		lazy = true,
		ft = "markdown",
		keys = {
			{ "<leader>T", "<cmd>TableModeToggle<cr>" },
		},
	},
	{
		"kristijanhusak/vim-dadbod-ui",
		lazy = true,
		keys = {
			{ "<leader>D", "<cmd>DBUIToggle<CR>" },
		},
		config = function()
			vim.cmd([[
            let g:db_ui_save_location = '~/.local/share/nvim/sql'
            let g:db_ui_use_nerd_fonts = 1
            let g:db_ui_winwidth = 30
            autocmd FileType sql,mysql,plsql lua require("cmp").setup.buffer({ sources = {{ name = "vim-dadbod-completion" }} })
            ]])
		end,
		dependencies = {
			"tpope/vim-dadbod",
			"kristijanhusak/vim-dadbod-completion",
		},
	},
	{
		"junegunn/fzf.vim",
		lazy = false,
		config = function()
			vim.g.bookmark = vim.loop.cwd()
			vim.cmd([[
            func! SetBookmark(folder)
                let g:bookmark = a:folder
            endfunc
            ]])
		end,
		keys = {
			{ "<leader>e", "<cmd>Files<CR>" },
			{ "<leader><leader>", "<cmd>Buffers<CR>" },
			{ "<leader>b", "<cmd>call fzf#run({'source':'fd . -d 1 -t d', 'sink':'chdir', 'dir':g:bookmark})<CR>" },
			{
				"<leader>P",
				"<cmd>call fzf#run({'source':'fd . -t d -H --no-ignore ~', 'sink':function('SetBookmark')})<CR>",
			},
		},
		dependencies = { "junegunn/fzf" },
	},
}
