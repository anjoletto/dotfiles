return {
	{
		"stevearc/oil.nvim",
		lazy = false,
		---@module 'oil'
		---@type oil.SetupOpts
		opts = {
      default_file_explorer = true,
    },
	},
	{
		"neovim/nvim-lspconfig",
		event = { "BufReadPre", "BufNewFile" },
		dependencies = { "saghen/blink.cmp" },
		opts = {
			servers = {
				lua_ls = {},
			},
		},
		config = function(_, opts)
			local lspconfig = require("lspconfig")
			for server, config in pairs(opts.servers) do
				config.capabilities = require("blink.cmp").get_lsp_capabilities(config.capabilities)
				lspconfig[server].setup(config)
			end

			vim.diagnostic.config({ virtual_text = true })
			vim.o.updatetime = 300
			vim.cmd([[
            autocmd CursorHold,CursorHoldI * lua vim.diagnostic.open_float(nil, {focus=false})
          ]])
			vim.diagnostic.config({
				virtual_text = false,
				signs = true,
				underline = true,
				update_in_insert = false,
				severity_sort = false,
			})
		end,
		keys = {
			{ "<leader>ci", vim.lsp.buf.implementation },
			{ "<leader>ca", vim.lsp.buf.code_action },
			{ "<leader>cf", vim.lsp.buf.format },
		},
	},
	{
		"saghen/blink.cmp",
		version = "*",
		---@module "blink.cmp"
		---@type blink.cmp.Config
		opts = {
			keymap = { preset = "super-tab", },
			sources = { default = { "lsp" }, },
		},
		opts_extended = { "sources.default" },
	},
	{
		"williamboman/mason.nvim",
		event = { "BufReadPre", "BufNewFile", "InsertEnter" },
		dependencies = { "neovim/nvim-lspconfig" },
		config = true,
	},

	{
		"williamboman/mason-lspconfig.nvim",
		event = { "BufReadPre", "BufNewFile", "InsertEnter" },
		dependencies = { "williamboman/mason.nvim" },
		config = function()
			require("mason-lspconfig").setup()
			require("mason-lspconfig").setup_handlers({
				function(server_name)
					require("lspconfig")[server_name].setup({})
				end,
			})
		end,
	},
	{
		"nvim-treesitter/nvim-treesitter",
		--event = "InsertEnter",
		lazy = false,
		config = function()
			require("nvim-treesitter.install").compilers = { "clang" }
			require("nvim-treesitter.configs").setup({
				sync_install = false,
				auto_install = true,
				highlight = {
					enable = true,
				},
			})
		end,
	},
}
