-- keep centered after navigation
vim.keymap.set("n", "<C-d>", "<C-d>zz", { noremap = true })
vim.keymap.set("n", "<C-u>", "<C-u>zz", { noremap = true })

-- keep visual selection after indentation
vim.keymap.set("v", ">", ">gv", { noremap = true })
vim.keymap.set("v", "<", "<gv", { noremap = true })

vim.keymap.set("v", "<leader>y", '"+y') -- copy to clipboard
vim.keymap.set("n", "<leader>t", [[<cmd>silent !tmux popup -E -w 90\% -h 90\% -d%:p:h <cr>]]) -- open tmux floating window

----------------------------------------------------------------------
-- light mode for presentations
function presentation()
	vim.o.background = "light"
	vim.cmd([[ colorscheme quiet ]])
	vim.opt.relativenumber = false
	vim.api.nvim_set_hl(0, "Normal", { fg = "#000000", bg = "#FFFFFF" })
end
vim.keymap.set("n", "<leader>p", presentation)
----------------------------------------------------------------------
