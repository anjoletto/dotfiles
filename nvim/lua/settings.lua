vim.g.mapleader = " "
vim.g.localmapleader = " "

vim.opt.expandtab = true
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.softtabstop = 2
vim.opt.smartindent = true

vim.opt.relativenumber = true
vim.opt.number = true
vim.opt.cursorline = true
vim.opt.scrolloff = 5
vim.opt.signcolumn = "yes"

vim.g.netrw_banner = 0

vim.opt.swapfile = false
vim.opt.backup = false

vim.api.nvim_create_autocmd({ "BufWritePre" }, {
	group = vim.api.nvim_create_augroup("default_group", {}),
	pattern = "*",
	command = [[%s/\s\+$//e]],
})

vim.opt.colorcolumn = { 80 }

vim.cmd([[ colorscheme quiet ]])
vim.o.background = "dark"
vim.o.statusline = "%f%m %r%h%w%q"

vim.api.nvim_set_hl(0, "Normal", { fg = "#FFFFFF", bg = "#000000" })
vim.api.nvim_set_hl(0, "Pmenu", { link = "Normal" })

vim.opt.spell = true
vim.opt.spelllang = { "pt_br", "en_us" }
