#-------------------------------------------------------------------------------
# main configuration
#-------------------------------------------------------------------------------

# terminal configuration
set-option -ga terminal-overrides ",xterm-256color:Tc"
set-option -g default-terminal "screen-256color"   # set for colors
set-window-option -g mode-keys vi                  # vi keybindings
set-option -gw xterm-keys on

# tmux speed
set-option -sg escape-time 0
set-option -sg repeat-time 0

# change to another session
set-option -g detach-on-destroy off

# set activity monitor
set-window-option -g monitor-activity on # show activity in windows by default
set-option -g visual-activity off  # no status line message, only color change

set -g mouse on

set-option -g focus-events on
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# appearance stuff
#-------------------------------------------------------------------------------

# only set the colors, no bold, italics, bla, bla, bla
set-option -g window-status-style none
set-option -g window-status-current-style none
set-option -g window-status-bell-style none
set-option -g window-status-activity-style none
set-option -g message-style none
set-option -g status-style none

# sets the background as black for everything and foreground for each stuff
set-option -g window-status-style fg='#585858'
set-option -g window-status-current-style fg='#FFFFFF'
set-option -g window-status-bell-style fg='#FF0000'
set-option -g window-status-activity-style fg='#777777'
set-option -g status-style fg='#FFFFFF'
set-option -g message-style fg='#FFFFFF'

# borders colors
set-option -g pane-active-border-style bg='#000000',fg='#FFFFFF'
set-option -g pane-border-style bg='#000000',fg='#777777'

# status bar colors
set-option -g status-style bg='#000000',fg='#FFFFFF'

# set clock mode color
set-option -g clock-mode-colour white

# status bar configuration
set-option -g status-interval 1
set-option -g status-justify centre
set-option -g status-left-length 15
set-option -g status-left '#[fg=#FFFFFF]#S'
set-option -g status-right '#[fg=#FFFFFF]#H'
set-option -g window-status-format "#I#F"
set-option -g window-status-current-format "#I#F"
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# shortcuts
#-------------------------------------------------------------------------------

# prefix key and nested prefix key
set -g prefix C-b
bind C-b send-prefix

# popup window
bind-key t display-popup -w 90% -h 90% -E
bind-key s set -g synchronize-panes
bind-key b set -g status
#-------------------------------------------------------------------------------
